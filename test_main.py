import pytest
from main import app


@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    yield client


def test_index(client):
    r = client.get('/')
    assert r.status_code == 200
    assert 'Hello World' in r.data.decode('utf-8')


def test_logs(client):
    r = client.get('/logs')
    assert r.status_code == 200
    assert 'OK' in r.data.decode('utf-8')


def test_user_upload_file(client):
    from io import BytesIO
    # case 1
    payload = {'file': 'teste'}
    r = client.post('/api/upload/testes/nameimage', buffered=True,
                    content_type='multipart/form-data',
                    data=payload)

    assert r.status_code == 400
    assert 'nenhum arquivo recebido para upload' in r.data.decode('utf-8')
    # case 2
    payload = {'file': (BytesIO(b'texto;teste;'), 'test.csv')}

    r = client.post('/api/upload/testes/nameimage', buffered=True,
                    content_type='multipart/form-data',
                    data=payload)
    assert r.status_code == 400
    assert 'tipo de arquivo não permitido' in r.data.decode('utf-8')
    # case 3
    import base64
    image_bytes = "iVBORw0KGgoAAAANSUhEUgAAADIAAAA2CAYAAACFrsqnAAAABHNCSVQICAgIfAhkiAAAABl0RVh0U29mdHdhcmUAZ25vbWUtc2NyZWVuc2hvdO8Dvz4AAAAqdEVYdENyZWF0aW9uIFRpbWUAc+FiIDI4IG1hciAyMDIwIDE1OjU4OjQwIC0wM1GdBM8AABp+SURBVGiBZZpZrKfnfdc/z/Zu/+Vsc87sHs94i+3EdeIkTZMSt4RWlFJBAwJRQIIKkOCGXlRiucoFFVIvKBW0QigSpRsNlVKaVGpCEhyndUgcpxnHy9ieGY9nO2fO/l/f5Vm5eA8pElfvxV//d3me3+/33R7x1bf+TRIxIYRAaEmMEa01ZVmSBEyOp5w+vUVtW6pBiUDinCX5SJFXHBzNcbMjvvGVV/jTF26wMhhxsDflocuPIRUMBoIrT5zigx/5KKcvrCJMQoiI955iKMAqxoMxPrQ0doHRhpAiAY9Njq5rsV3Edx5Xd9i2o7M1trW4tqNtHF1YoIfDIfPJlLIscdGT5znGGOq6RijJ6dNbmDInuhYpJc4FTGZwwbI4bvmNX/5drr58DyklK5vnuX17ynjtLN6OuHX7Jh/+yI8wfbDOi3+0z/CU5ezpC6yvbXL73ht87MeGDFchJQlASv2CZlLTxoAUEmMMAkg+4WW/yClURA1RRrSOdEEgvvHuL6dMaY6Pj8jKkqIoCClQjUq6paMcD1Ba0dQtynrylSHNYs4f/tYX+fLnbxK8IhA5feoc0cOwOsXKmdN853+/BFEyGBVcvHCJMxcuQZIMBmPG4zFnL1wk+Y5F8yY/+w+eRkZNpCamBEBUkaZb4kmkAHXdEtqA7zy2qWmams7WhC7QuhapVUVSEp3nFIXBGInOM3TSzOslKSVsZxEukA9Lmrbld/7D5/jy57bJzTrbd6cc7jn29mvm846d3Qe8+eobuNYyLAuCS1R5zt6Dbe68+w4He9ssJsd89+Vv0raWYfkUv/Kv/4jjowlSaEgJJSVKCEgCow0AykiU1qACWmu01uRZhTGGGED9/C/81GdskBQmQ2cGkkAJw2QxY1QNaLuOGCJ5ZnB4Xvofr/C5/3KVhGL3wTFSarZOn8Zay6Kumc0XLOo5vmsRUhK9JS8E2/fu4W0iyzNee+17lJnGCMnW5iZ3bx9SLzWnr1TkpSSGiBCCItNY61CZpms6FIoYEykGYoQYHDFGBBH18//8Jz4jkDgbMWWJ7wTSZGRZgdSCrMhIUtC1kRe/8AL/9ddeQOsVjg6XeA8hetq6Yz6b47uOTGtynTEeFIwyycMXTjM0ES8EUgm2Ns9BSLRNTVVmXL/2FuWowraaQaEYbUTKKiO4QEIilMYnj5CKFBI+eAgRYiLGhPMeKUAv6znGaIwpSK4j6oiUCu8hRoGKnpgiN996hz/47ZfxLufw4BApBCmBQOKDZWVQcfHUmEGV89DmOoOhYn00ZjGfs74+wCfJfgPbh/s89uij3Ll7m821McvphOVsyng44ht//A4+nueDP2oAQUgeJQwqZUhaUBKtDEEnvAsIJTFGY5NAj8ardG2ND5bONlRVRfIduSlQec5yOefujTvcu3YXFdY5ONhFIogpEWNkUCg+fPkcn3zucaqqJFc5RVHio0WrHM5tIYUm5hkXk+Tjpx5GDtYp1fNsnt3k7vXr3N7e5vc+/wWeeeZZvvLfv4+1l/jEX3qKEBUueKQSSCEQAYRWJKtAglKKICVKe7RWgpRlCCFQSmFth9aa4Fv2DueMzJDjBxN+69f/jOXSImIkIokpMMw0//DHnuXixU2KvMSYvjG1lmgyUuprXWmDzAYkJTi7tcVw7TxnHrpAsEsGwXNqtWD3+Mf51jf/lPc9cQXaksbVSGGQUmJdi0wJKSUIj5AJrQ0pQZIORES2jUUI0U8na6mqiqZraINjUOTMJlNKvcJ4bXRSbgHvOjaKnH/8Ex/l3Nk1BoMBWZaRZTlZZpBaYEyO1hlaFxA7UrBUeUW7bMiFY350QBETMrUMM8lqDq1rMXnF69/b52CnJUmw3iFkIimBUpIsk0iZkFL0mKMNUka0kIlm0dD5jpWVFbquY1AN8SkSgqVZTPjsv/tjJpMakKQUWR/m/M3nf4jzF08xGo1QJ6suhAGZUEoQvELKQEoBZSoEnqaZIaXh3m1Llhsmg4JBPibGyPragGeeeooHO7tUs4zvfQOe//RTCBVJQYCOhBBJKSEVeCkQApAgpUb6IBiuDCmKgsGgIsRE27YAHO0fMD9ecni4ILp+zAkEj58e8tD5VfLckGmDNhlK5SitMToHJFIkpAQp+zHpbUDGwGx5yGy2Qz07ppnXtL4GIqOqZDgoscHStg2vv3IDKQwhJKKIQCTFvoyElAiRCMIhhABAGiWZz5aMRyvMF0uKQUkxKAnO4r3jl/7lHyCFoW0sWhu0gk999EkGxYAir1BG9WVkDFJqhBBIkSFV3y9CSbQpESZDCIVEEYKjcy3Bd9SzOZ11DHJNbgT7B8e4IFlMJDdfexdIZEYSA0BEiP9LZRRKFyilEAmkVAmTKRZ1jS4MXdMQRCAJzb2390hOMpnMUULjuo5TKxmro3HPybQmRUgCRJKIJJBolJBIKZHKoGSOEAkhLdYtQCZEFATXsVjOaOoZ1rZImWjbOSaTvP766xRGc/vmLvWio2s8JtP9PaUkkBAyoIQiSYkyGrmYztHGkEJEBMjzHAK0k4Zf/5WvUY6GVIMROlM4H3loc5WsrPoSOhkSEvGDqZdSJMa+ltPJiO53yaCkQQtBVQwo8wolFUmA6yx111IZye7OPo898SivvvEmx/tzTCZIKuF9JEZBSBGTSaRSQM/LhFDIKKBeLslyjdKSzjuO949YzOY8fCVnMbM0TYt1jkRia2OMNgZtZP+CWoFQJy8d/pzFJoEQCiEUKQlSUmitiTH1GATY6Om6jkhCAJfObZFcw/7uHpcvPcx7b+2zc3cPpSUJ+nGeBD5EiIIkQEaPVBlSa40PgZgS0/mChKAsS377P/0B5y6eZb6c44Ijpr7RTw0kEk0UoLVGmRKhDULIH4xxIQRSyv/vmtLJB4cOT0TriqwYIpQ5IYGa3BiWbceNGzdpJpZuEnBdRAiJNtkJEBpSSiileqwREdm2LWVRoKSgygu0VCTneefaLm6eoZWmyAwigdKaIiuRWiLVAKcHODKSGCBMQZ4PMaZA6xylDEpmSKkAQfJ9uYUYmbUtSxvxmeG4tURhqPIBpiw4vVbxYPsBR9OGg+Oa7Xs7HD44JqX+/30pJaRWpAgA3jp0VVXkec5sMSPPcwZZyXGX+Hs//0k++++/RYyJYB2ZzljM5mRZyf2dA9587zWeeOr9jAYDytwzqAoyIqOiwHctyP6hKQoECtt17O3tcGP/iHf3F9zdPWBra8x4uMLlc+f4yHNPk6LiA09e4bX39gghcO/uPseHE1ztaRuPMQoSxOgIwZOSICpIKqGttYQQ0HkGCqbTObZZMF4b0NSelAJZZgCJT5EEHMxa8vEaL377KvePDhnlJUbChx65wLNPPs6ZUxvE2Nd9jBHnOnYP7vHK2zf49lv3KEzG6uqYC+MBWnv27t7ixiByZusUj10+g3UtKQrWViu6hcL7Fp0JYgpY2yGlJgmHADSCjojWWpOiwEhB8AEpFFVWsKwdSNHXY5QIAlrC2nCIFxXn8pLnHruMd5Zrb15j/3iCr2c82H6PcaYwRY6znoTHtkuW8wNyFfj4Y5e4eGYDk2fowiCUxNklOswJbUamNFIYkgStFQc7M44fLDn7WCDhQSRCcPiQEMkTk+9pirUds/mEum5JSVDkGbPFEts1ZKogzzMSHqM1JstYG45Racpk+12+//2XefXadVxekBU5N2/d5aUXv0m9WGDrCYSapj7m3u5NvnP1FpVY4dzjzzF47tN8/s9uct8N+LXf/Bw3rt/CSIk2GiEVLjgSkdl8DrID4QnK4Vwg+oBMvpcRoh/fMUZkWZYIITg6OiKlxJ07d8gyzcvf+D5d1+I62xsAJ/pDKcna+gbbh/tMFp4f/cm/zhdf/A5f++41fupnPs3P/PinmM/nGFMwXFlHGA1ScvH8GNnWXDl3js/+219kcv8OK9Lzs3/5eT7woWdYPbVJVhS9wdFZfHAMB0NOXVhhUI2JTY9HIfQfE2ME0Xd7jBE9PZqxOh4RGCJDYmNjg3q+YGDOYtQtXIgEFzBlRmuP++klNB/+0LMcHB7y7otf5Bf+1qeJ3YzD61epCs3pCxeR0ePtHCklxaBktL5BubnC/ddf5G984oPY6MndAWcevcxoMGLt1BbOO+r6iBQdH//Y89y7/Q4P9u/z9tsGvfYImxdOARBSJAkInSOEnpnoFB3OWtrOkucZgYDSgqPDB2RG43EUhaHUGUYNe3SNESklF89fxJ1W2NZho2Lj8mMoDZWGvCoJMaJVRmWGXDiV07rEaOsKIba9rZNJsnLA6tpptALnHSEEhMx59dVXAcfa4CIQ6Q6XpPMbSAnBAvTYFEK/O7qzjvn8gPOXziOCJ0pNu1gyWMvxYYnROSlYHBohweEwGLKiJNcGXVaIlYKYPNG2SCzSKGJKff0KT1GOIOsohCb5QFWOEFIiMzDFEOFbfIRAYjlfEmOHtRYpEvPFgrHTeO+JwaGSIaZACIEU6Use0BsbG3jvUVHhcSgMbVtjFORZhvOJajxGWoGPFpUiCEmhh+TlEKIjJYdWEqkrOhtwzpFlGVEJuq6lCzVC5CQ0hamQkhOXZIxLCesDQUAg8huf/zpS5r2AC46NzZKm7pjPFxD6hUwxIoQipAbv+96Vtu0wpn/56GAxbzEmZ320htI9VxJBkoTEyAqP7NVMEmghEWi0MiANSSmsdeRlicoqEBnV4BRGjQldS/Rd36RRoIuSJBJSCFyKhBAI1vHGvf1eX0gocsgKw+HhHitro973+n8IqUT0i0JExxiRCOq6RmeGzFTMF3Dv7gFGKpbOUjcOgmNjdcRb797lh556Apkb8qwArYg+QIz40GFyA1KjTYVKof89RpLSTB68h14dYPIMmSCKngjGFAmxo65r6BpkWSKkhKQ5s3ma4BOnL57GdRZ6A5XkLMEDKUFMyBA9Ow/uk+c5Sgrq+oAYLMH1JFFKkEoyGAwZlQX/6+pNlNJ470gatFRonRGix7nAaDhkXI3YGp2hytbIpEEpRSYU1XiVGBNGGUgBEVPfrKlnF4t5TTUYEkUghoSPcPPGbVLM8CS8D/jokURSFMTY76SPAbm+tsH62haj0Yj7d+4zHGwgk8a2kjzP0UqzrGsWywWCxM0HBySRiMmfMFoB9BokLwucDRzN5hzO9+h8zXQy5YUvfYnpzgP80rO2sdYjscoI3uO9hRAJ3vKtV98leIcWCikkg7Jk70HHR/7i00gpUcr0xlwQhOiIyRGTRwiBbmtHDJq2iZw9d4UYJNoMCF7S2N7Cj1HgpGTWdHi/wDUdSknarqHIJCEGdJEhEcxqz/TwGGMGHB0dsVhOuPLoI8isYPXUGO+WoIuetgaHix02Jdplwxde+A4SAQi0Fv3Cvb1NMSqJ0ZOSRSUIwRFjQAlDSC3BBaR3JaNyi7JYx1mQcogSiqb2zBcLjOn9qUE1oGk7Mj3gpVev430gBk90NZGGZdNwuJgxqVu+9vqb/NHXX0DIRAjw8vXbvL2/x/3jQw6XS97bvc/2/gOWzRLvE3W9pHYdEoHSmpQCIfRW0M/9/Z/GNTX+BGO89yd0XtC5pt8hEtr7OfN6RggJpRzLxnF45xbe9a74xuqY3YMJznWQoCpK/tvXX+YTH3ycup7hpOGobrh67QbVaMRbt27xYH/Gc1fOUM/3yfMBTddxd2eXd+4csDHKybUBIzm/WnJqY526bfml//x5ICeRkEITgqcqK95+/TZnn3maUmvkid8rBAgCKTpiEhBBd26KDII8z+m6Bi0l1TDnYLrASIntLFIqMp0xLGOfRUjBl799lZ/84fczzCuuvnObm9v7KLnLUBk+/YmP8pGnnuD+/btUVcnf/tGP09iOF9+8Tts2HLdT7uzO+bHnHiIrJAlDEhVKC4L36EwzGo0YVgV3rk+R9FZU9BHnO4RIdJ1DKYV3EYlHi+SYHC9ZP7VCUWakKNjb2SfPJaUZsewahJDMFjO0UdStRUnNF7/5Js8/8xhGaS6tD3nfuRVe//7bnB+tkh3VvPtn18EIHty8B0mwc3zMIC45v7VCURo++aHLhJSYN5b/+HvfICSBFOnEwBAURtO2NV07xNcdViRIDufcCc6IHu1th4+xF99FpsElooK2textz8g1HM3mrAwrlt6SRB+P5XlGDhxP5/yrz/4hv/yP/hpnN9dIKfHjn/wRmkNHJcfIoGjahotnN9DGsLJ1xNTtISqHUnA0n+OC4OtXt/ExkKJAyp5uCNFbril68kJzfLjgVDYmREsKHVJnpBCJXSCFjuASUmmDyXJCChghmewfslh2KGXIpWRQ9BPGuUgm+gRJIkFJmtbz1e/d4Gh2RG0tQkCxKgmqQWAZlBkqOerFBBuntKajsZbbe1N2Jg1vvrfH9bv7hBBQskfrJPrrvKmJKTBbNHzxd9+mbQUxFgSnCK7FOcdiOcEGTwgCnZLs4V5K5sslZ86c49Z4m2pQkELAxZ78pRhJUlM3UzJTMiwGTO2cL7/yBhfXhzz6MGw3SwbVgPu7e7z23ja70wVSZmgdwXZcvrSKzDJu7005nEbevb9D1/WaIoQ+UhMJtNF45ykGOUeHC7Ii40u/P6c0Y37m7/wcd++8hFS3QAz41Md+ERUCOsQGJUpi7Mi05HDvPjpLxGBRUrF/MEFpRYiRFCNVppnWDVL2nqvzid//5huMvmt4eGuFJ8+f4Yee+QBNMNw7fI3hQFEvF1y6uEErCl565Tbz2gJwZnWFt2+/h1LD3lnPshMjLhJTou0s1SDD6JK7dx7wT/7Z3+Vbf3qVZz/4w1z93pCPfPj9DFY2yKYH6CzLmR4dsjoeQUyMVzJs6JAqIy9zxHEDIRFSpLWBvMyJS4t3fdACUFsHCN64d8T2PHDgYFQYttbXqG1guLbFjZ0Zo7Gj8YmiMIiQmExmKDXsw06l6KxDCtkHRdZxenOTzEhs26FGI/7FL/4CFx5+lM9/4UusDofUneXFP/k2F/02OnYepQzORXyM3Nte4dWXewt1b3cfJQ2j4ZB5U5PnGmUyUpz8QHZq3Sexhggm5+7+PruTI0qToYUgGYULEW89YddjMsMjZ1ZxyyXXH1jAIoRGSE0IkTNnzrC3u4PWCusi8+mCUV5y4fzDPPL0x7h96xYHe7vUMbC1sY6ynvdtPon2ViKlprYFv/ebN3nw4BDsjI2iYDhMHB0v8UTKoqTrOmRISKkJ0fdOn+x7LFeCi5sDroVITILD6QKlDSFatCpQsu8zg+eR02Nu7dQ95c9zlNIMxmNmkxl13QBQliWbW1ts37mDlJLj3R2KkHHlyhO0XeDRMvGp0SoHv/rr3NrZQ/3VTz/1mc997oivfu09du/tIL2lKA0ydLR1h0edMNSIjwFvPW1rCQmUAik0QoAKng8/dppPPvYQrfMsvKfIC6K3lGWFjwFk4p/+lQ/S1pavfPcOgXQSvIbeE46RPM8oqxJrPfVywZNPPs2d+3cZ5IpaZozH6zzxgSdIgxEvvHaN54c5D1Uj1N7y1GcOdg9pFy3L5THee4Rz5FKQ65zJosV7T54pYoS66UBJhIhIaUBEcm0oMs2lUwMeeegi73voApdLgwgRZzs+cHbIX/jAZX762UvsTib87p+8ixein4QpURQVQkqCO1kwH9A6Y3VthXq5pChzSJ7DgwmXH38/+7s7OB/YOH+Ol0zBu5cvo13nmM1nxM5RZhnWWtomUq1vEtqWcVnhQiBEi3eBEP887hKij7Bj9JT5gOnCEmLAaHj6ySd5/NEGkeeE4JgcH/Kr//MVFktPIvUnG3SG966nRpk5uW9vKJACxWAN4RZcOHuBGzdeY/PiZcrScPV7N/nYR38Yu+gI3tNmOXr//h5ZpqhdoAkWVwfytQFHRxNWipKERyuIUZHnGcuuOxFcAh96FPZBsDNvMUaz8t7d3t2PkZWiorGeq3f22Z57UtKk5HvnnkSIASF6nte2NcPhmMWy4fz5iywWU/Z3tjEqYLICdEVa1rzz9jUWk332ZzOs7Vgfr7J97x5i68JDSWd9diFEIjrIisTpYcnGcMDxdEHTthxMawSB2sYTJJZY17+IVIayKE7CF9BFRvQBcaKtrQ2kFFFSIaTE+14RBm9PMhOP1IZlveTU+iZKZTTNAu8DRiUuX3mU8doWcbHH+pVnOZjPmUynPPPMczjb8vq1N5DqJJ8rq4quc6xuDokh0jU1tuu5f9d15JlCSoWgpylZVp6cfkjEEGg7i1CaiIDUx24egfWRJAQmL07kam+qpRR6Bao1xuSIBIXJaRrLbHqE1oaV1QHeBxbzKVbl7E7mHN55h3KwyvVrr/YAai3RWmSVVwiZsHVDSol6XpNOGmHZNYQUyLMCI/o8QguBTJDlGQiBkBqpFOVwgA99YuViwKeIURqTlSjZH1ZTJ6FPWeYMhxVCnBgHkn5XJBgdewzpGmaTOUIIprMZXd0yWDlN3c5JBOg62rZlumwYVgVysZhhjKGpazZPb9DWLVle0thA07YoAUlEijwn0J8SigKcs4DEmJyyHOBdr7+VUoh4kh+mhIsdCE4MakEUEqMloteD+BCJPuFsoCwKbOsQCR56+BJKwtr6Bstlx5333mK0dpbjeUs7m0IMHE+OCckzGIyQgdRHz3nG7LhGKcN4fUw+GNI0HdPZkvmixQeHUYqu668kKMoCrfu+EFJiTIZCkOUZRp7YQC5APx4IgMk0zkec740+pSVN1zJeWaWzHh8TPgS2797BOsd8vugto7ZDaMF0smS5mJFnBXs726gUOZwcI6tyiFIKk+doCabIWUyWzOZL0BlkGUZlKCUYFjkqz3puBH2aRUJn5geOfUyRtmlA9GpPKYUxmhQCWZahhASlkVIRE3TOkmUFi8UMIVKfJZ48Y1iNSEhW106BEtzf3SbJPqePIVDPD5hNpyATsmsaVkYlmj4yWF1fpXUerwxnP3QB6yVRKtrOE6LAti1aKbx3oATGGJTRqJMdsURSTDjbn90SSiGQoM0PcnIlJVJrTJ6T5QO0ydA6w/qAMZoQwbqAA4TWBBLRedxixtkLD9Etl2idcbx/QFZVdMsl/wdu3CQ0iEgy2QAAAABJRU5ErkJggg==" \
        .encode("ascii")
    payload = {'file': (BytesIO(base64.decodebytes(image_bytes)), 'teste.png')}
    r = client.post('/api/upload/testes/nameimage', buffered=True,
                    content_type='multipart/form-data',
                    data=payload)
    assert r.status_code == 200
    assert 'OK' in r.data.decode('utf-8')


def test_cep(client):
    import json
    # case 1
    r = client.get('/api/cep/04304050')
    json_error = json.loads(r.data.decode('utf-8'))
    assert r.status_code == 200
    assert 'Vila Monte Alegre' in json_error['bairro']
    assert '04304-050' in json_error['cep']
    assert 'São Paulo' in json_error['cidade']
    assert '' in json_error['complemento']
    assert 'Rua Francisco Mesquita' in json_error['logradouro']
    assert 'SP' in json_error['uf']
    # case 2
    r = client.get('/api/cep/0004304050')
    json_error = json.loads(r.data.decode('utf-8'))
    assert r.status_code == 400
    assert 'cep invalido' in json_error['error']
    # case 3
    r = client.get('/api/cep/testeCEP')
    json_error = json.loads(r.data.decode('utf-8'))
    assert r.status_code == 400
    assert 'cep invalido' in json_error['error']
    # case 4
    r = client.get('/api/cep/00000000')
    json_error = json.loads(r.data.decode('utf-8'))
    assert r.status_code == 400
    assert 'cep invalido' in json_error['error']
