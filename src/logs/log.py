import logging
from google.cloud.logging.handlers.container_engine import ContainerEngineHandler


def debug(msg='', ctx='cloudLogger'):
    cloud_logger = logging.getLogger()
    cloud_logger.setLevel(logging.DEBUG)
    cloud_logger.addHandler(ContainerEngineHandler())
    logging.getLogger(ctx).debug(msg)


def info(msg='', ctx='cloudLogger'):
    cloud_logger = logging.getLogger()
    cloud_logger.setLevel(logging.DEBUG)
    cloud_logger.addHandler(ContainerEngineHandler())
    logging.getLogger(ctx).debug(msg)


def warning(msg='', ctx='cloudLogger'):
    cloud_logger = logging.getLogger()
    cloud_logger.setLevel(logging.WARNING)
    cloud_logger.addHandler(ContainerEngineHandler())
    logging.getLogger(ctx).debug(msg)


def error(msg='', ctx='cloudLogger'):
    cloud_logger = logging.getLogger()
    cloud_logger.setLevel(logging.ERROR)
    cloud_logger.addHandler(ContainerEngineHandler())
    logging.getLogger(ctx).debug(msg)


def critical(msg='', ctx='cloudLogger'):
    cloud_logger = logging.getLogger()
    cloud_logger.setLevel(logging.CRITICAL)
    cloud_logger.addHandler(ContainerEngineHandler())
    logging.getLogger(ctx).debug(msg)


def testes_log():
    try:
        debug("testando a log")
        info("testando a log")
        warning("testando a log")
        error("testando a log")
        critical("testando a log")
    except Exception as ex:
        print(f'erro: {ex}')
        return "Erro ao imprimir logs", 500
    return "OK", 200

