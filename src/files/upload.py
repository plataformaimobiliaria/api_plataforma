import firebase_admin
from firebase_admin import credentials, storage
from werkzeug.utils import secure_filename

firebase_admin.initialize_app()
bucket = storage.bucket('plataforma-imobiliaria.appspot.com')


def upload_binary(name_file, file):
    try:
        bucket.blob(name_file).upload_from_string(file, "image/png")
    except Exception as ex:
        print(f"erro ao tentar efetuar upload {ex}")
        return "não foi possivel efetuar o upload", 400
    return "OK", 200


def upload(request, key, name_user):
    if len(request.files) < 1:
        return 'nenhum arquivo recebido para upload', 400
    if 'file' in request.files:
        image = request.files['file'].read()
        filename = f'images/{secure_filename(key)}/{secure_filename(name_user)}.png'

        import imghdr
        if imghdr.what("", image) not in ['png', 'jpg', 'jpeg']:
            return "tipo de arquivo não permitido", 400
        return upload_binary(filename, image)
    return "não foi possivel efetuar o upload", 400
