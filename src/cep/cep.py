from pycep_correios import format_cep, validate_cep, get_address_from_cep
from src.logs import log


def endereco_cep(cep=''):
    try:
        cep = format_cep(f'{cep}')
    except ValueError:
        log.warning('valor cep invalido')
        return {'error': 'cep invalido'}, 400
    except Exception as ex:
        log.warning(f'erro ao formatar cep: {ex}')
        return {'error': 'cep invalido'}, 400

    if cep == '' or len(cep) != 8:
        log.warning('cep invalido')
        return {'error': 'cep invalido'}, 400

    if validate_cep(cep):
        try:
            endereco = get_address_from_cep(cep)
            log.debug(endereco)
            return endereco, 200
        except Exception as ex:
            log.warning(f'erro ao buscar cep: {ex}')
            return {'error': 'cep invalido'}, 400

    log.warning('Erro ao consultar cep')
    return {'error': 'erro interno do servidor, por favor tente mais tarde'}, 500
