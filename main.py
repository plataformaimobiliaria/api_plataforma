import os
from flask import Flask, request, jsonify
from flask_cors import cross_origin, CORS
from src.logs import log
from src.files import upload
from src.cep import cep

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/', methods=['GET'])
def index():
    return 'Hello World', 200


@app.route('/logs', methods=['GET'])
def test_logs():
    return log.testes_log()


@app.route('/api/upload/<string:key>/<string:name_user>', methods=['POST'])
def user_upload_file(key, name_user):
    return upload.upload(request, key, name_user)


@app.route('/api/cep/<string:cepnumber>', methods=['GET'])
@cross_origin(origin='*', supports_credentials=True)
def busca_cep(cepnumber):
    code_cep, status_code = cep.endereco_cep(cepnumber)
    return jsonify(code_cep), status_code


if __name__ == "__main__":
    app.run(host='127.0.0.1', debug=True, port=int(os.environ.get('PORT', 8080)))
